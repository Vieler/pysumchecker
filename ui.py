# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_sumchecker(object):
    def setupUi(self, sumchecker):
        sumchecker.setObjectName(_fromUtf8("sumchecker"))
        sumchecker.resize(400, 300)
        sumchecker.setMinimumSize(QtCore.QSize(400, 300))
        sumchecker.setMaximumSize(QtCore.QSize(400, 300))
        self.main_layout = QtGui.QVBoxLayout(sumchecker)
        self.main_layout.setObjectName(_fromUtf8("main_layout"))
        self.dropBox = QtGui.QGroupBox(sumchecker)
        self.dropBox.setAcceptDrops(True)
        self.dropBox.setFlat(False)
        self.dropBox.setCheckable(False)
        self.dropBox.setObjectName(_fromUtf8("dropBox"))
        self.main_layout.addWidget(self.dropBox)
        self.sumBox = QtGui.QGroupBox(sumchecker)
        self.sumBox.setMaximumSize(QtCore.QSize(16777215, 104))
        self.sumBox.setObjectName(_fromUtf8("sumBox"))
        self.verticalLayout = QtGui.QVBoxLayout(self.sumBox)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.layout_algo = QtGui.QHBoxLayout()
        self.layout_algo.setObjectName(_fromUtf8("layout_algo"))
        self.radio_md5 = QtGui.QRadioButton(self.sumBox)
        self.radio_md5.setChecked(True)
        self.radio_md5.setObjectName(_fromUtf8("radio_md5"))
        self.layout_algo.addWidget(self.radio_md5)
        self.radio_sha1 = QtGui.QRadioButton(self.sumBox)
        self.radio_sha1.setObjectName(_fromUtf8("radio_sha1"))
        self.layout_algo.addWidget(self.radio_sha1)
        self.radio_sha256 = QtGui.QRadioButton(self.sumBox)
        self.radio_sha256.setObjectName(_fromUtf8("radio_sha256"))
        self.layout_algo.addWidget(self.radio_sha256)
        self.radio_sha512 = QtGui.QRadioButton(self.sumBox)
        self.radio_sha512.setObjectName(_fromUtf8("radio_sha512"))
        self.layout_algo.addWidget(self.radio_sha512)
        self.verticalLayout.addLayout(self.layout_algo)
        self.txt_result_checksum = QtGui.QLineEdit(self.sumBox)
        self.txt_result_checksum.setAcceptDrops(False)
        self.txt_result_checksum.setAlignment(QtCore.Qt.AlignCenter)
        self.txt_result_checksum.setReadOnly(True)
        self.txt_result_checksum.setObjectName(_fromUtf8("txt_result_checksum"))
        self.verticalLayout.addWidget(self.txt_result_checksum)
        self.txt_checksum = QtGui.QLineEdit(self.sumBox)
        self.txt_checksum.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.txt_checksum.setInputMask(_fromUtf8(""))
        self.txt_checksum.setAlignment(QtCore.Qt.AlignCenter)
        self.txt_checksum.setObjectName(_fromUtf8("txt_checksum"))
        self.verticalLayout.addWidget(self.txt_checksum)
        self.main_layout.addWidget(self.sumBox)
        self.main_layout.setStretch(0, 10)
        self.main_layout.setStretch(1, 1)

        self.retranslateUi(sumchecker)
        QtCore.QMetaObject.connectSlotsByName(sumchecker)

    def retranslateUi(self, sumchecker):
        sumchecker.setWindowTitle(_translate("sumchecker", "PySumChecker", None))
        self.dropBox.setTitle(_translate("sumchecker", "Drop", None))
        self.sumBox.setTitle(_translate("sumchecker", "Sums", None))
        self.radio_md5.setText(_translate("sumchecker", "md5", None))
        self.radio_sha1.setText(_translate("sumchecker", "sha1", None))
        self.radio_sha256.setText(_translate("sumchecker", "sha256", None))
        self.radio_sha512.setText(_translate("sumchecker", "sha512", None))
        self.txt_result_checksum.setPlaceholderText(_translate("sumchecker", "Calculated hash will be displayed here", None))
        self.txt_checksum.setPlaceholderText(_translate("sumchecker", "Write the supposed checksum here", None))

