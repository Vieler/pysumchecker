# -*- coding: utf-8 -*-
"""
pySumChecker

This tool is a simple program capable of computing checksum of 4 common types,
namely md5, sha1, sha256 and sha512. It uses PyQt4 libraries as GUI toolkit
so if they are not installed in your python distribution, do so before
launchin the script.

The icon in used as application icon was downloaded from
    http://thenounproject.com/term/checksum/21048/
and credit for it goes to Iconathon.

author: Vieler Hyloks
version: 1.1
email: vielerhyloks[at]gmail[dot]com
license: GNU GPL v3
"""


import hashlib
import os
import sys
try:
    from PyQt4 import QtCore, QtGui
except ImportError:
    input("ERROR: PyQt4 was not found on this system. The application can't start.")
    exit(1)
from ui import Ui_sumchecker as ui


BLOCKSIZE = 65536
supported_algos = ("md5", "sha1", "sha224", "sha256", "sha384", "sha512")


def b64_to_image(base64_encoded_image: str, img_type='pixmap'):
    """Uses a base64 encoded image and return an image

    This function uses a base64 encoded string representin an image and returns
    either a QtPixmap, QtImage or QtIcon (defaults to QtPixmap)""" 
    if img_type == 'pixmap':
        return QtGui.QPixmap.fromImage(QtGui.QImage.fromData(QtCore.QByteArray.fromBase64(base64_encoded_image), "PNG"))
    elif img_type == 'image':
        return QtGui.QImage.fromData(QtCore.QByteArray.fromBase64(base64_encoded_image), "PNG")
    elif img_type == 'icon':
        return QtGui.QIcon(QtGui.QPixmap.fromImage(QtGui.QImage.fromData(QtCore.QByteArray.fromBase64(base64_encoded_image), "PNG")))


def h(file_name, algo="md5"):
    with open(file_name, "rb") as f:
        m = None
        if algo in supported_algos:
            if algo == "md5":
                m = hashlib.md5()
            elif algo == "sha1":
                m = hashlib.sha1()
            elif algo == "sha224":
                m = hashlib.sha224()
            elif algo == "sha256":
                m = hashlib.sha256()
            elif algo == "sha384":
                m = hashlib.sha384()
            elif algo == "sha512":
                m = hashlib.sha512()
        chunk = f.read(BLOCKSIZE)
        while len(chunk) > 0:
            m.update(chunk)
            chunk = f.read(BLOCKSIZE)
        return str(m.hexdigest())


class DropZone(QtGui.QGroupBox):

    def __init__(self, parent=None):
        QtGui.QGroupBox.__init__(self, parent)
        self.setAcceptDrops(True)
        self.parent = parent
        self.setTitle("Drop")
        self.layout = QtGui.QHBoxLayout(self)
        self.lbl_file_path = QtGui.QLabel(self)
        self.lbl_file_path.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_file_path.setText("Drop the file, whose checksum is to be calculated, here")
        self.layout.addWidget(self.lbl_file_path)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            self.parent.current_filename = fname = event.mimeData().urls()[0].toString(QtCore.QUrl.RemoveScheme).lstrip('/')
            self.parent.ui.txt_result_checksum.setText(h(fname, self.parent.algo))
            path_width = self.lbl_file_path.fontMetrics().boundingRect(fname).width()
            while path_width > self.lbl_file_path.width() - 10:
                path_components = fname.split('/')
                if len(path_components) > 2:
                    if path_components[1] == "...":
                        del path_components[1]
                        path_components[1] = "..."
                    else:
                        path_components[1] = "..."
                    fname = "/".join(path_components)
                    path_width = self.lbl_file_path.fontMetrics().boundingRect(fname).width()
                else:
                    break
            self.lbl_file_path.setText(fname)
        else:
            event.ignore()


class MainWindow(QtGui.QWidget):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = ui()
        self.ui.setupUi(self)
        self.ui.main_layout.removeWidget(self.ui.dropBox)
        self.ui.dropBox.close()
        self.ui.dropBox = DropZone(self)
        self.ui.main_layout.insertWidget(0, self.ui.dropBox)
        self.ui.main_layout.update()
        self.setWindowIcon(b64_to_image(img_data, 'icon'))

        self.algo_radio_buttons = QtGui.QButtonGroup(self.ui.sumBox)
        self.algo_radio_buttons.addButton(self.ui.radio_md5)
        self.algo_radio_buttons.addButton(self.ui.radio_sha1)
        self.algo_radio_buttons.addButton(self.ui.radio_sha256)
        self.algo_radio_buttons.addButton(self.ui.radio_sha512)
        self.algo = "md5"

        self.current_filename = ""

        self.algo_radio_buttons.buttonClicked.connect(self.onalgochange)
        self.ui.txt_checksum.textChanged.connect(self.onchecksumchange)
        self.ui.txt_result_checksum.textChanged.connect(self.onchecksumchange)

    def onalgochange(self):
        self.algo = self.algo_radio_buttons.checkedButton().text()
        if self.current_filename == "":
            pass
        else:
            self.ui.txt_result_checksum.setText(h(self.current_filename, self.algo))

    def onchecksumchange(self):
        if self.ui.txt_result_checksum.text() != "":
            if self.ui.txt_checksum.text() == self.ui.txt_result_checksum.text():
                self.ui.txt_checksum.setStyleSheet("* {color: white; background-color: #09A687;}")
            else:
                self.ui.txt_checksum.setStyleSheet("* {color: white; background-color: #cc0000;}")



img_data = """R0lGODlhsASwBOcAAAAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6ent7e3x8fH19fX5+fn9/f4CAgIGBgYKCgoODg4SEhIWFhYaGhoeHh4iIiImJiYqKiouLi4yMjI2NjY6Ojo+Pj5CQkJGRkZKSkpOTk5SUlJWVlZaWlpeXl5iYmJmZmZqampubm5ycnJ2dnZ6enp+fn6CgoKGhoaKioqOjo6SkpKWlpaampqenp6ioqKmpqaqqqqurq6ysrK2tra6urq+vr7CwsLGxsbKysrOzs7S0tLW1tba2tre3t7i4uLm5ubq6uru7u7y8vL29vb6+vr+/v8DAwMHBwcLCwsPDw8TExMXFxcbGxsfHx8jIyMnJycrKysvLy8zMzM3Nzc7Ozs/Pz9DQ0NHR0dLS0tPT09TU1NXV1dbW1tfX19jY2NnZ2dra2tvb29zc3N3d3d7e3t/f3+Dg4OHh4eLi4uPj4+Tk5OXl5ebm5ufn5+jo6Onp6erq6uvr6+zs7O3t7e7u7u/v7/Dw8PHx8fLy8vPz8/T09PX19fb29vf39/j4+Pn5+fr6+vv7+/z8/P39/f7+/v///yH+F1N1cnByaXNlLCBtb3RoZXJmdWNrZXIuACH5BAEKAP8ALAAAAACwBLAEAAj+AP8JHEiwoMGDCBMqXMiwocOHECNKfAigosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKDDmxps2bOHPq3Mmzp8+fQIMKHUq0qNGjSJMqncm0qdOnUKNKnUq1qtWrJJVq3cq1q9evYMOKHUu2rNmcWNOqXcu2rdu3cONmPUu3rt27ePPq3cu3L0+5gAMLHky4sOG0fhMrXsy4sePHkOkenky5suXLmOFG3sy5s+fPoEPXzUy6tOnTqFNjFM26tevXsGOHVk27tu3buKfK3s27t+/fwIPmHk68uPHjq4MrX868uXPWyKNLn0598vPr2LNr3w62uvfv4MP+6+ZOvrz58+gVil/Pvr37kenjy59P3/f7+/jzr6/Pv7///4rpJ+CABBIH4IEIJqigVgU26OCDmS0o4YQUVggRhBhmqCFgFnbo4YcJbijiiCRSBeKJKKZYXokstuhiSyrGKOOM9r1o4404dkTjjjz2yFmOQAaJo49EFmkkXkImqSSJRzbp5JNbLSnllA9CaeWVWO5E5ZZc5pfll2CGmVCXZJYpnphopomlmWy2KZ2acMZJpJt01pmbnHjmmaKdfPaZmp6ABiqhn4QWepmgiCbKn6GMNkqYopBGuqKjlFbqlqSYZtqcpZx2epWmoIa6m6eklvqUqKimCpqprLb6kqr+sMbamKu01nqSrLjmmpetvPb6ka7ABjuWr8QWe5GwyCa7lLHM+qrss9AC1ey0vUZr7bU1Uattrdh2661624bL6rfklivuuaWWqy626Lbb6brwPuvuvJXGa2+w9Obb6L38yqrvv4X2KzCqABfc58AIZ2rwwnUm7LCiDEfc5sMUAyrxxWZWrDGcGHfc5cYgh+nxyFSGbPKVbZ2MaXQqt2xkyi5DzHLMNM8Ic82BTofzzifezDOe1P0sNIU+D61mdUYnfWDRSouMdNNQz8d01Gt6R/XV5k2N9ZPgbe01dlp//XJ4YpcdXNhm97hf2mzHhnbbNq8N99yrskV3ke3drff+Zm/vDaJ7fge+WN+CW/he4YjrRXjigx7O+ONmLQ45gvhNbnl3dl/uoZead56U5J7Xp1/opA8FeunxCYj66n9lzjrlA74u+0Snz85dgbbnzlDtuoONe+/AE8R78Js2SHzwwx+vXJXK5558879BCP3sz0/PG4bWs1599m5jzz3p23/vmobiex5++aJtiP7l56//mYjuQ95+/D+OSD/i898PWYn6B55//7Oy22kAqLf/ETBAmRvgAeFmwAX25WYKdKDZGihBxbnuHxGsoNcoqMG7FC2DHaQaB0MomQsKBIQkVNoIU1iWqaGQhUJbIQzForUXzhBnMrzhV8JmQx26LIf+PuTK23oYxJABsYjLWsvuTIPEmB2xiUchHBGh6LAnUpEoi5viFQVmxS0Kx4TgKo0XNdbFMfoEdFo047rKqEYtgXEhaWzjt9goR5zULo51vBYd85itNy5RjHyM1x4DGZHh4ZGQyBokIh2SvEMuUleKfCQc/UgRJkpSj5S8ZFi250hNEiyTnvRK+DoZSk1FspTCA2UlAYlKYJ2ylRhU5SpJA0tIyrKWRpkfKXGZs1vy0nS+ZKQlf/lJJRKzK//b5TE5Fsxl9sSAynSmmF7ZygZGU5pfoiYqKXhNbKKsmd60IzgLOcxwWmyc5qQdOi9UznTKSZullGE33ekjeIYyh/P+pOeO7OlJIOZTnzLipyaP+E+A7mmdBp2kMZFS0IR+SKCXtGJDHWo4hFL0IF2c6EUbt9CNosWiNtGoR2HX0ZGGFKQnZaVJ54TSke5RpCsVXUs9OkiYxlQ+EJWkIm16U/Tk9JGR5GlPJ1XSoQqzqMhsp1EfOtONvlKoS/UdUqM6pqbqBKpULd5Us2oQbWKVq2ezKkXh+VWw1mirZj2hWN2o0rQuba0J5WdZ3Qqbny4SonOlK3TgatCc5lWvdUNrWn/6V8B2xq6ItGthDRsZxBISsYtlrGMcG0jHRlayg+MrQCl7Wcz6hbJ8BG1nPbsX0ObRtKMlLZI0q0/TxrKtqnX+jmvlONvUxvYss21jbl9Ly9tqVS2k3S1vI+Tb5Qh3jMe1bXF3yFp6Hne4mFkucJ67ReoqV7oMaq47qQvdQ2F3VNpNJ3e7a5nvyma8UETvdc2LxfCaE73krQx7x+fecMI3vpSZ714Fa9b74tc6+g0scD3rX7XCNsCPKbAPFfzfwyC4fvwFK4MbbJgHN7a+3pwwhQtj4QRjGJsa3vCjOsyYELPQxOsl8SwRQ+APM/fAKl5thLlqYgP3NsaldbEza2xj4uJ4VzpeJo97HN0fy3jAmB0ykb1r5NEE+ZhKHkiKmyxipwT3yTRUKpUxN+OsRjmVMN6yELH8yy+D+cZifjH+kiVr5jP7OM1J7TJV2+zmIsN5zHKOKp3rzOQ7J3HNjN0zn8vr5z+zOMlkxq2WCy0UQQPQ0YOWL6NzmWhcQjrS+Z10e/O81EsXZMr69TT9RI1pAGv6J6R2X6qlvOhT32TV6IP1kgntarYC2rCynrWka/1RThs117rONK9fXelaAjvYph62On091GMj28HKXvatAevsZ1c42uRkdk+rbW0OY3vFWLmytqfb6m9/utiw5HapoW3uqo47pupe97XbjVF0V9PeCAyzueOtPH7L29v0/vepWvxu5oA60PiOZ8Izq29l+5t4Dz93w3kdceBVXOCDCXiVmyLuact04qe+uO7+RI5xwdCb5LZDOatBzmiVy87lK0fzsGGuvYXzrdwtt/lAdX5hlqeZ5qsDeszf7Gqhl87o3R5xrZEeOqZvPDAU57lOpQ5hmee84CZ1etIzHnKq39XrVSe6n7WuObJv3eSTNrvl1P50Dl3d43Rl+9mhPnawP9bu78N5jOX+OL7P3e0/x3toBZ93nyPY74lD/N/lEnisu5TwAhY7jhVfOMovPi5btrz/ID8bvQdY834DfdsZb2TRF5Dz6fM8e01/N9Yf3LmoVyPrL68ZFc9+brcfPeZJnPu29f71GY69GXuv+9o/mPhpQz7wdyx85Db/NcuH8vOrO336Gp7gcHcr8of+bufVV5+K2+d+n78bfrGVn/Zvme/5N/j9uqqeze1v4vrFT+vlzh9r90f/paSbfxHG/7zvF3f/V0T9p38FyH6OR1MDCIDXJ2ELuGAP2D0N6GUReEMHSH+71nGHBn8JSCPRB1QVOEMXWHJ0h30biHAduE8BqGchCEMjKHFWp1cvaDQzCIOSN1gteGI5eD0reFM1GEM7yIMTmHVBGEI/2FU9SIQpOFZFKIQxSIFL6FBHWG9DeFFTuDNXiIRVKIVNqEFZqIVP+GtdWEFfCIY32GxjKEFlaIbdx4JRGFdpeFZhCG9xuEBriBAfKH91eEB3iIdJCHtv2Fd7GD1/KF6D+Gj+hyiHZ+hUiag/fRhGc8iEgbhZjaiIbfh4k9halWiJ46eAmQiI2WdKhch8n7hdm0iIWyh9pWiIq1g1qVhmp6hqsUhukWiKrWhfs0iLtYiLtxh8vZgleeiFuVg+jyhti8iLodhfwxhWr6hwvyhNxdhHzdhPy/g90SiNu6iKyeiAz+g00zh13Shk1Whco0iN4aiNJJMhoHiCuJaO8MOK20hj7viOyMiO1DaP9uOL8QiF+Kg+IDaO2dOPTAKNAGk9AjmQ4niOxHSQCLmQBTk9DNmQvHSNIBOREmlsDwk9FnmR96aQsLiR9IiRHjmRIJmPIrmPc1aSJtmRKOmGKumPLGn+jzL4kiHpjC3ZaTQJk9uUkc2TkzppkzIpgD7pPTEZbiY4lMaTbjzZb0gpPUo5kpbWlMxTlJ9ylFKpOk95k2J4lb9DlVahgVw5OlkZlNoXlgRykmSJg2aJlV5ZFWC5lo7TliZilXCZN2NplIhWl5Vzl1WZl3oZlzsJlWj5l+wxmH3JgYRpl3I5Hn6ZmHITmFqJho5ZmHz5lXQ5mU+zmFLxlpiZmZCZlsrYmWRTmW55maI5M5oZFZx5mqj5mXiJmKwZNKQ5l40Zm28ym4wJm7bZmkD5mii4m7eZmlCxmsA5HIZpmbVZnMVxnKWZnMppnLi5mab5nLbBnLSpm9R5J9H+qZrTmZ2qYZ25qXE4tJRRQ5Hi6UHkCTXmeZ5OJpiOmJ7sSVKRKXvwGZ8Asp6Qgp/2SRb6mSj9uZ+bVJ9J858AqmagmVgCWqD0QaC95J4KCioMek4O+qAKk6BDE6EU+jkWCoQTmqGRgqF5AqIeWhQi+k4bOqLkUaJxoqIoKi0nyjMs2qJn9KJYSKMyeh0xmiY5eqNXZaM1s6M8Kk4dKotDGqRA46M0A6RGio0HendFuqTM9KTEiKRQiorzOXxUWqW9oaRgwqVaelRX6nxS+qVdmqUt46VkqlBh6kVomqbutqbUN6Zu+k1yyj1tOqdseJi9qad46i13aiV/2qcGyBL+4CmdgjpHZqoygSqoi+okjYqnj3okkeqmk4o3iXqohQenV1SpZMqp9XSpmHpYoGpEoxqqPaep4FeqpuphdRqQqrqqAdSqBvmqsMpwqJpetFqrieGpapOruvpAvlpFwfqrFiSrEDmsxHpkTVpZyJqsdsGrPAKtUCqtHtiszlpCxqqR1nqtkbOt/EKtRgquAeWtoWKoFkiu9iKuvwV4DqSuKuKu4BWM+IeugkSvwGgc6wOvKKKvovoddmqvawSwn7qXx8OvfyOwcTOVzoOw5GKwOcaRR8ewiJqtwjokQSexfoqxHKUk4KOx7OKx91kmneOwmwOyi2Ink0OyHaKyUWL+KIzDshUCswxVL4IjsxNis8BEKqFnstGCs6jGLXTjswoitLZmK7jHs9BCtEJqLGyjtG9Fsa4kLmXjtCELtbgyL19Dtf+htX+UL/NqtRCHtJzoteUptsrCtXlqMOpptsmCtiSItTTItokkt/vljhdKt/iCt5najz/jth93q98KkjUKtsijt6dakj9quLlCtE3pRIp7tY+bb1J5ppEbKzgLlyfjtwtauQ+LuaRKuBbHucqqlxUpuqmisqJJRqZbTIDbs7FJMZorNavbQrv5MLGLU7ObZcqZMLebOrnLZdQ5ML2bHurqncfSL8PrU7+LZ8ZrEYELur1Drc27EfeSvOf+Ia3TqyPwYr1Zs7wzm70csb3euzLju2ng6xHqwr1E1bqqcr4iYS7l+6Hx66LuSxMTy75BFKn1axIZC70jN7+ts78l0S3qm6IA3KMCzL+Y5L8Ly8DvmsApYS0FfDsHnFIskrNC4roOTD0VbIwKG6AukrQd3KD4KzNdyRcQu7gjLKEljCixE3YO0rYrrCdeSrCt8cEqvMEvN8NqeiZWKpZRq8Ovo6SPaXBAnMMtfK5CPLSjScGA6S88fKRLLJ86g7uKCcVTjDox6pkn68NYnMQiGMUYqJ1M3DVfvKyDl8X9EZxE46+WK8YrKsbIcVCy2b5wHKVg7I0GkrC8Wa53fDT+PIyvvdrHoqjGHWvIvkvGY3Mcp/vHOrrC0AklcywqE7wdDKrIrhjJhZzHOojI3Vudj6zJ5OvJI1vBtyHFuAGhjowm/1kbJAzKo8zJKdSftJGfsCwplawd+lnLuOzKsYzGpzW/36nKwyy/pFx28fsnrIsavXzM7FO+ymzH0eyfqzxN48vMSPyNjlrNegzMcazNNCyvVizLJHSN5WjM2cjK3FymzkyO6SzNx4jH3kxbvwvO6NyJIbrO2ZS79tzMl4jK86xbs/vOwhLP6tzOKbu6BD23+PzN5GyECv3P8Ft/AO2b92S6Eh2wGVjRfGqOD72vDZ2uG+3QAU2fCM2AI83+RcLG0cjpmh3N0kpnu8lmoifdd5Ub0ggz0/Js0R5d0lyT0hXDbjT90cJY0zcM1Kob0yTN0ztn1HU7b5Sr1IDs1PijuEj9uWg31D4tpkTdxjrtuCW41C8dUYa70oNLelrN1GRN1Z5h1jDKrjs91uDY1Qvi1n2L1mLd0nuq16EMcCq0e2kt1yDI1jcH1U0D2HndnC7N193MdVdjfIl9nXut2Act1QYMI+Ost7mcHRco1Jwt2br8g5stVXRdtZbtzqsm2vp8r4Q9WX69PK+N2mptza29s7Vtq1mti1cdr4Ld2L2NoLe9q7HN2/0s3LNN26Vth0g73O53y05I2ZEdnhf+HdzAetpPjckSCNrRba6Trd2Aat2RN8nZLd3bzZ3COXD77NjWZzXjPZyvfNxfR93Fmtupd8XXzXGCMto4CrLgXdj3sd7ozcJbzaYe29/7c5b3LRPULN9Hy+DoSd/9mpSdh98uvNqZPOAHC+H+7ZTh/SomnNx8qLHqDcPq2OGEassO7nsYO+KHm8KseuL3DN/ADeLKG9YHfiMmPsC/LONOSuOfDNeuHSQ5DhLE7OP9s30sXmJSMuFETskWTqdG7sR4HeRLXt8aASv6/RzKp+HGzSXPnOJNi7BcXt0im9BgnnwCO+YoTCfy8+TffeYgPOWfxSc2DedTC7BqDmR+knj+bi7JeA7ka+6yldfnP23ncSbnncsoNUvo22zozAvZgU6ze5PlskWvNp7oljLpjN4kt3fpeq6zrbfpkoqunj66nhLqjo6AUV7piD3frtLgqz5q5Frq7Wm0Kp7qW+N6iP6sxHLrsR4/pkfrilYsYY7rX/vrzAjpD860EyTqi4zsur1/n94sd27sj72tgI6t2mJ+zm6p1o7B0r7s4ZK13c5S3/5Fra7t56LqGL6p1rrrtAu3137ua0vvM5ru3Uq2/mfvf83vAZx+4k4v8w7tsdas8K67ALPvBD+l/o7AAF/rBVO2DX+3E7+04T7sDHPY5T6wC9/cD6/uGT+gGz/IHQ/+ffg+LB4j8hV/1yvvwTqXjnHb8mfd7n978fzJxS5PyEk68tEarCcf54KM7jr/QzyvgjLPTsqO8KJMaUE/8zzOrEcPbkO6nMCbym8d9TuP9Q3x86KE3VXv3GBd8v+q9T28cKcc8MWc9WLvqmQPiWTly66e9kTf9opKq1x/6OI8xgYNu0VfrXRPhTb/9QsN8ucsvn9vMnyX9AY6+GjP+NXb93xM88Sr+F1f3Epv+Rq89rN6+G9rZTeP+Zfv+Iav+cfK+XoP3YtP0VYO+jJs+hvDdndvaKoP4Bkt066f1KS/4Q6690P+1Xx/+0GtqpTfsjjt8bWf05A/rqYf+9+72yj+ffwqDfy/n/s3/lSzn+zFj7zJHyNqN/yyz9yyff3Cu/0PzPner6F2reXQ//jSb/vUT+WVJv7qn/0i3f4V+/6xGlTp/9n0P/qSr4cAAUDgQIIFDR5EmHDgP4YNHT6EGFHiRIoVLV7EmFEjQ4UdPX48uFFkRpAlTZ5cOFLlSpYtK6KEGfOjS5o1bd7EmVOnRZk9Q+4EGlToUKJFjR5FOtHn0pRJnT6NyFSqQKgOp15VWFWrTaxdE24FG1ZsUK8wx55Fm1bt2q1lUbKFu9KtWbBzu8bF+8+u27x9/SbdC/LvYMKFDR8NPPPw4MQl6zZeuvgs5KuSLV9+SRkhZs6dPRv+1rz589rQWduWfjsaKmqfql0zZt309WzatcnGpmr7Me7cWnkL1m3098ngxcP+Np5c+fKHyJkjHg7gdPSvz29T72hdO1Dn271/t9wdvE7q07EXHJ/zfPb07TeKdx9f/nHe87kON7++t32W+qvzB1Cv+gIksMCd4DOQJPyq8o+gBFVq0KAH5UNwQgsvVGpADDNbcLUIN9QoQvRA9K5CEk9M0EQUo/PtQxQvEtHBF5lTcUYb46uRRBYZdPHGDGOUzsfichSySOuIxLA8Dxs08kcRm7QNSSinjFLDG3dcUj8qowJyS9ek9DJMz8CcEEunehSToy7T5IxMNt8szM0UOwT+jEk41XzyTsnk1LNPtvgk0Mw6tfRzTT8JA/RQRenDTUhBofNPUUMXzStRSi8908oZHxUu0kVjxLRSTUMldbJRV7S0JTs/BbVU0k51NdYsWfOR06I8vXRSWRmNbVdfeWz0SjohXY/UVn/drVdkl+00WBttHQrXUI9l9qlUq8WWQ2WfvRZCQl3NM9tBtxW3XJq6dQ9aoaT966fawjWXKHTjFXfe9NTl7tu4ZPoSTXrzdfZfgRUMGFVYoy0WLqzG9HdgnOx1+FeIwRv2Vn1N5QszeCO+r2COP25o4hIrljdhtBrTuGGQ5Tp45YFF3g5f8kweq7TwVnW5v5Zzphdm7Uj+Rvg8jFHbE2eeRfL56EOTfg7o67Cr2Wm8VFYaxp2rzpbp5WRWj2ZguX6VXax5unpsZrVWDuybvJ5VSUTFNttJWuP+F+3kpD5Q6PzY7stourn0+G9s7TYO75mhbpHavv0WPOSyG4+V8CElFxDxtjcW9WLI8SR387MfD9Twh/W2FsgR24W7cco9FzbwC0Vfm/RMTZfQL8ZVB531XHMHEPaO3R6X9tMz51vw1XV/8XjafK9JdqSEN23x4v9WHnkdebdP7XMtDx762qV3HnfXrS+1+n6xV5V7Yr0XjXj1xe+c/MjRp1Dy8C1mnz33gYd/bvl3NZ9qmOeS+wUtf/qbmub+6BbA/z2IgZ8ZYPq097sDIlBhCozbAxtYIA12JoIs41/JKgicBE4Pax3cYO/oly77hdCAI1RMCd+XwRWm0EsovMwHveXCp8GQhBc0odJwaMP6jW9ONXwPDwHmQ5PsT4cOGyIRWWhEAz0RaUrMGxOJsy8MVi2KUrwXEsdDuAJmUYtbBOIMTyhGMBbpi6AhIxa7dka6pFGOOXtjG0dGxdCxESNqPBwdU2PHCUYsj3r8mR9jZjdAzlGQdfxTEF12SEQ2TZGJvCRFGjm6R8aEi2XkGSUrubVMWrKUErkjBTs5yLBtcmWiHOXdTklKPjYvldtbJb8+eUuOwTKWk6ulCoP+SUBeSjCXniSkFcvly1/qhpky9N+6CrnDY+oykq782DObubxZpq2bVikmCKvZk2Rqc1Pf3CYH0QnM+AVynZwbJzKvGc66vTOd/DFnOSGDP7TFkyn6bCce7XnP+eRznjYTYT/9GZmDKnNwAyUojiD6roByUqELbQ1AibbGYUaUWx3NXkVj51CrYTSjrZzmMifq0TGulJsIfaFIVWlSeaoFlL10KUv3KNMqblSaJCUbTU9q05tCMac6xSRPe6oZfq5TqFLRKExDeVSkmlKp6mRqQkFKzadak6jYrOdWq5okqr4mq1q9qs66ylCUGpSsYh2rhdy6y700S2trhWpby5r+prnG9W1wDWli7ApYguF1qGkpaljT6te3LhardlmfY0di2Lx+NaXV6itjbbfX2dQ1stE0I2W9iliwmiuzmnViaByVscFK9orS0tXQgCqr06KWrj5t3cKeN1u58VBxsuXslmpr24ZKtVZTKd1dTYi5qNETs8El7vlc68B/JhedXXTc7Xg1XGFON7r4hK4zR9s93IYWdlTbLncD693vFjS8wWGldQn7x8TCs77yfa8b89velM23sd9Lln+1NUHtBni/xxUwf1uaYKM6tbTgxO7eGGysAyt4Meo9p4Mvm7r0TnhaFbYwHD2sWPaW1IoFlnCJ5zfiENMIxHe6LH1jXLn++86Ot5jCcIsvp1rd3Rhw3eJwikH7shfreLMsNq1y5xXhxPmYVUg2MjuH3L8pj3TJTN5xleOV4yjvFsr1YtqMIYLlLBtXYFzucmvNTOXyOnLEZLYxmr8j5zTHdM3UUzKL4RznL+uJznXuYZuNF2Yn09i5TS3yfwUN6PVqmYbXLfSevVzoPv2Z0e5c9AIJ/U1Jf9bRWUv0pU8WajFR2r6ZtuWDJ21pF/dZ1BAkdZjyrOIBm5qYtn4Tq1+daloTuc9iDuqh0dprSul617cmds827WpD41qcxi5crI/d5E9z9NeMrPGqmV1qaU+7zPv0nK2dfeptTxbYxe62t/l850f+X3ugkvz2WbecbnVrm91mmzWqMV1hYX+43PWOE72btOxkm3jc5j64cAUO8GHzeHMEr7ab39jv3f2b4aizOG0hvdJse1rfyIL2xWVc8CS7O+PNPjDF0X1ykd/23tYmeXNy2nE1f1xiC2+5eV/uRYjvfIkTP3ehcJ5zifv8aOI+Ks0bbnMADp3oVo740TUc88ICPeQUZfnTSZt1f1Mdwl43OL+D7menax2XYP+cqwOoao87HMxcN7tYrg5ek6M92FYvO8XyHvdnM32qdY96oOHeW7tTae58J7e8Bw14o/804Uk8vAD3jvjXFv7mjHf718Qe+dFwPu6enyLaNch2RAf+vnyTp3yIUH89zCte84NH5dhzvfrUh73xAm09ZeQue6hbfuC0r32tbz/Jjfv+7il//GphH3x7Zx7PuQd3h03fdtcvC/Rav34YoY+y3Se/6sZXPviZL2TdP7z407cz+kvv966rf/wiFj+4zs/+pQ+/5vZfefzfj1/8Z3P+/Rc8+qO+6Huu5du/9Ku+dnO/7wPAnzNAmcu+onnAA3TABmww0csvldM5AXwy/aPAARQs89u+EASuBXS8CSwT4PvAxCs/yOk55zMwDxQ+CyQ7FFzB3qPBM/s/GIxBDqy/BNQ4G7xBXjPBCyzC4/PBE5RBTfK+5FHBFYxAvVO7/dLAfcv+wdkTwiFEtiPUwSkUQt4jQi7EwiXUwg0EwrHJtyssuiRUQjasQTIswzU8Q5hzQ9vjQfKbw3gjwMuDwzjEwTtEw6kTQytUQ4vqQ7l6wg+Mwjl7QUB8vUE0REiUtUSkwEXcKQxEvi9qwhOxxIvrxEUaQRIctSgCwyn5RIA7xSPZQUd8xDo0wzzEMUo8wFS0Kkn8sUOEwEzMwu6yRT9sRVgUIkHsxTB0RUIExvwbRl9cN1bkOS/URRvcRBChRW+bRlqyu1LcQlzMrl2kO21Uxr4rxHlbxXCcKXIkRnOEkmo8NnWUpXFER2PyRhZsQV9hx1erRylLxm2MxlvMR7XiRvf++sdvtMNjlDpnRK82LMZITMgbksX9u0d8fEd9jC2EZEbyikj9CkiBnEGC/LtQhLdzrEiLDMmlacj3e0j4SkPT+cE97L6MbI+TBDSYrBJhFB4E5Mj7u8k37EeNtEmWXDyDZB+KnMfmcknti0eeZMCR7Eig9J4AzMmV5D56LMnxk8mZZMqmfMWhbMmdzC2uRMqsjMpwS0mslMOn7MmwDMKj/EokVMql5EomMkatlL62hLGpZL6qxDqaDMqylMsepEs4wcsoC8zOwjUtiku03EqvdMKiXEuJvEhQeyfDVMi/PEvEPD3GbEyUo8xXKky4nEyzrMzA4EPFzEyQ7MufZDb+ySzHzXTK04xFzGzMwSTMi4KhzwTN1vRJCoPNtZTN2dTLvVzN26xA1uS23fzK3jQrZ1NN08xNPGzOilPL0uTHx4TMyPTM4HzOXxROhjROpERO6bLO2vzD7TxMy2y/hZROoczO57On6zw70oS86JSo7uTJ7wRPy3PPbKROrkLP36NPjbRPyRs3HxpP16S2/jSSAG0vBYW1ARVP5jRPvzTQDoTP9ARH4uRMB62gAl1PPRTNpvtPgWTQkQOwxPS9EeLQCHVOUZS/EP3GESU8C9LO/URR7FTRA91Pg5FPC5XHG11MSJpR8hzIibxQIeVLH5UUuww+GP06csLRE82fFGX+UaKs0A1hUtS6Us2sqWUkzSi10Sk10RxlvR3lUSYtCw+d0C/9yKQ0UttMU6FzUWWE0blAUyRVU9Jjws0j0wXb0/QcUc8SyS6tSTcF0zBt0wRV0tpj0EJVTzHV0jVVvejMUin9UB4tLgx9STslVExlU6XbyA6tUzpNyyq11Ejt07+qVBA8VLb01Bhd1TuFrFFF0FKFVVHVUUbFTUftUT0DsUmtVdai1VE81SN7U0p91U89pTzyVQjF1WAN0mY9omL9VWgFy1lV1kSVwGHNzO+sUEg1VlBFOEnF1gsbV8Tj1lnNUzx9Tyi71jhFSXf1Q+SMP2/dVGotUmkNVHDVSXT+dVZ/JNX7PFZXhTuPjFXL+tcUhNc4lFfxC7L1S9VqfdgS5NR0LFe+W9gq7TSIrYzQBNSt09akOth+jb2EfdKAHdlWrVe24tiC9diQjVaXFdlchNk2camGzdfDylVN5VJ8BcyK/Tyf/VZbVVV+vVcgXVmWFdaZ7aOP5U2gnVav4L99ZNUYar6JPVJ7rTSnfbreVB4Uk9jh2VmeHVqk1U2mPU6tXVdddUxshEirvdqIPU+1jVmB1VeAZLmD7Eb5XNa0ddu5LVqdtVsPxNvAVdqTLVxeJFq/7dS6LaKsYy7ETVx4jNxbnVzFRVbAbVwZfNxGO9wxQ1vw6dx+vVjH3dz++XyxvdXPvrXc+AxdcqWq3yLcyv1brN1XuV3dtTXZaHtd2A29f0RdybXd231Uxs3ckCVSo+TG3wVe1RXeISVe0+U6leTc1sVdsR1D6qXVcy0r6YXe5P1c0JXd5m3S8M1LMuTeTKUk5fVX8hXf4cXc2GXf8fVaaywy9Z1duG3fRqXdvDXf82VEqZ3a52UT+40o7d2rQf1fAL7c/a1LktVC+wRgBAbZRCPgcMXeUoXgCJbgVlPgdDXbWozf5s3gByJL+k3fCq68C7bUEQ6uEtbdKsxZBu5ZBx7CAGVbU/Xfd71hZsXfuGXe/DXcEPbN5fNSHd5hvs3dj/rg+vxeHn7+3/LM2MOI4rEl28tcYgBtYiQOXpndYAFVVyoW2hW7YhHNYi1O4sUd3Gz9Ypy03usV4ttVUBhO3RzurykGY2AVYxUu0zKe4y3mYuhhGJuV0DP+UT220EU9YjTmXVS1YzYWYDf2Y8tF5A7e1dJNrTV25Cee4TF+UT7u4x+24AMKuEX+WkIu5Dde3UmmZPelHYwjZUMFZQTjZDn15E82ZRKt0Uud30FuY0iOZSCW31/m4CGiI4Ol45Y15N5FZUmu5eUN3Ueay1eG5Vum3EhW3DmVYycu4jtG2UzW5E1OZunE5kRm3VyK4eNN2ln2Jhq+wXEmZxxeKP48o0sW5q5c5mv+bmZbpmZcFq14pud95sR8XkeBXt9n7Gd/+meAHtNwLs0rxeThPOhVAl91XmeKVliCLugsjGiEhiZ2Fi+PVkSMvt9ePtqNBk6XY2j0tegyzNJuPmeTdmFdvmdEBOlKFOmRfmSohOmY1quVfuGUjs2blufT3enljCqFDuiankWhHuodLWoCnWif/umZnttJpdebfWpLplIUdmaSBmZ4tuaPzmYozuqrDtUeltWwrmqmbmoyLWtAjmqpnmq1jtllNeuqfWuX5uYqlkqldki2bmtDzmtp3mrALuWc/mpFRusMe2idHmyqDmDIpmm5hkLDDuVVptvHluwFrmeMpOyQ9uv+ww5nzX5nM95shP1smw5t0T7tICbtztZmxM7a1b5Ly75szPbg1/ZqrIZtzwbqpqXtwn4m3e5t06ZrRTtu0bXt29Ym4v7ms35ukgxuRV1u5u425+7YuJ7uIW7t7K3uFB5rx8bu+NLu1J7r4hZf9RXkdB5vlSXW8PZlpIbj7wZv+C7p9m4f2NBrODXvv95u4TYo/MbZ8v7t6UVvEabv+i7tlBVwGWFk+x7gBOcvAt5lY27w6Hlw3JZGCV9QDgdrCL9v53bd/Zbu/jZJD/9wDU/xCx/xxqbQ7sZgFFfsAq9e/FZjEI/w/zZXGZ9xGOfn9r6Zu7ZiGmdiHWftEL3wIGn+cRd/8eR2Vq7W6oTO6zpm8iY/cOHlalZWZ+ymchwHZx/fYyNn7yov2aym2UYecjA/ZB4vZxLn5YgOZDdHRjX3UzZf8QUPcZNqUCHPYzoXZzu/cxXPaMrqvPV+KDE3uyyvcUEf9KfyYj7vcycPVkWvZOAjdIDF8+I0caoE9DaH9KPuJO4mc+gkcixGdAv/dJQO9ZdC87S+8vk+dVSX857Oz+Q09Lfb9NqOdVkfdVBH50ef9dfM9SXtdAVPdf3m6fINdmEvdTLedV7vdQJndPH284Wu9m0tduuOdmTX2ihXtmdvOUr33F+vaBlm9VYfzWsPanCH9m2P8y0t3mX3Yfn+TuVsD2x33yBvJzF1B+5hhz/CZimA33dJ9252p3V0ByOBH/hXr3eDP/hjtyGFX3h6Z2aHf3iI/x9yNyR7Rypxz22J1yON33iLrzeP/3h9D3mQn/jdTuzINvccb2WPOmbc8/fUM3nOxvdAVPmVl+2Wx3m+nvc0JqKZd0uCj3GS9/WcL0iUH/map7ybd3mE17Sd70Kn33GkV3Wm7zGRz1Crt1iOf9ppT/MKBxmobyOzj3qpL3qyL3uwD3i3D3uxT1Kib0av/1msl3K2x7eYd0G4L2C/j3u0n2aMVym8H2jDz3u9X3vFb3vEt0fAD3y513SuX/pm72TH7+hk3/suRk3+fj9bzM98zQ9Gzu98o19hyGdwxv920i99hq94uwf2ba58up962Mc+1H9bqj901mcz18dn0H/vDaV53m99iv9928f0k256uCYfwR963NdYygc52Uce54946I9+6Xd17dd55Cc668dp2u9r5W9+7I8l8A9/8W9R6rce9G8g97939t9+vn9/8x8l+I9/+S9b/a9++68k/AeIfwIHEixYEADChAoXMmzo8CHEiBIXGqxo8SLGjBo3cuzo8SNIixNHkixpMiTKlCpXsmzp8uVFkzJJwqxp8ybOnDp38uzpU+PMoBF/Ei1q1KXQpEplHm3qtObSqFKfUq1q9aXUpVe3cu3+6vUrWIJZlYYta7bj2LRjz7LlqfYtzbZy58KEy5Qu3rx69/60e5Iv4Kp+B/8NbNgg4cQUDzPWq1hi48iSJ9N9PJQyZpuWNy/OjJczYc+iuYJuOPo06tRQS3dW7Toja86vv8a2O/v2ztoJcfPu7Vo3Qt++gT8W3pS4WuPKVRJf7vz53ubQfyNXPB1n9bTXt8cEzv07eMHew4vObp08SvNr0X+Xzv49/Lrj40tWn5g+bPtT8UN3z/8/gN3pFiBj+t0XoIH7EWicfws6yF+DD+aVYGj4URiVhMJFmCGH4G3YYVsXVkieiBiCiNuHJ6qoXIorhlXiiNfBqJWLr7VYI47+1A2Yo1wzDvacjzTyiNqNQxqJWZFHWhXkj70xKaSSniUZJZWATVnlcU82mZqWUGI52ZVfislWmGP21eWWSKJJlplgztcmnJW9Gad4a6YZmJ0m0lngnHv6SVuff2aZ5509EqqnoFYGmiijTpXZqGaHngeWpOtBGt2il2rqVqab5lbpZk+B+panjnVaKqpInZpqpKPGtpqrfrEq546z2qpqrbdyGiuvvc6ka4irAjusSMISu5KvySr70LFnPdqsrs9C69Gy1So7bVnSYpuqttsCZS24o3pLqbHj2tqtuQKGu+6a6XqFrruMwhsvYuzayyS9pJWb76bz8jvQvQGL+O/+Vv4S3KbBBwu8sHoHX5Www19CDB9EfDF88b4R5zSxxlFyHF5JpmI8cqgdU/WxyUOiLKNQE5L8sqwpn5yxzHCuDCSihsK8s4I1D1qbz6XevJylc/F8dFJBi0qz0hIz/R5cLiM9NWRNO/q01R5jTaJtn1H9tWlZ//yq2PJu7WGhwYL9ddlX59r2nkPzNqnXax8N99is4f2n3LdZhqndMO99VN+DW3j2dn9bHDjGhhOOuOMdFq5jcYoyHnDkRk2eOXqbEwmaYZezy7nmkJPuoOenlcan6NeeXlTqr7P8NoBkh966r7LDbrru/8Uupe234w5q77vTXnyOv2d2/OLD54n+vPFAQ2+k8pTxrq/zXU5PVPXbo3i9huAXnH2Q3nMvvvntoT939xuTP3D6ZzIff4btN5bd8u/rR//58/OP+vr8hj/g6c9+yDPg/wgoPQQNsDwFDF4Ce4LACFovgLNpmOoeWDIKym+BHHzQBPGEwc9pMGYf7CAET8hA/0HNPpQrYVZUGL0UyhBCFrTR/nAIQzbVEIV66+EKPeg7Awlwh4UBog9Xh8QhspA9FGKfES+zxP4JcYotbGLnnuikKLbGikkEnRfjE0IRJohBUQzj47CIRpypkWtlJNoD15g3JcrRjVW0oRadk706uu2OfJzOGMn4xv4w7o9La6MhoejHw13+SH1IS+TMEAnJC96QkvBDW+MmWadFanI4lbTkJUFmr06OT5KkJCEn6eMjMbrulKVMpSt1CEtWwoiJhIoluUyJy/zpUpS1XBC+dpmtTwqTTMQEZYlAZJ5iGu2YzHyRM2WZzOSl7ZnN7KU1DxPI+60ymxHcpjc/hU1fziic/wOnObETTWROM53mQ6c7YTVLW5YzntODpz1xNU96/jKfvcOnP5G1ziKWL6CyA6hB0zNQgnYzoZxDqEM/AlHhBTOihpuoRTeCUUEWNJwVu+hCM+q+cVJMe9YMGd42KlJ10VCZaBJm0sqm0pVWZKaW01Ise6Y0m9JULCHdokk7WbSg8bT+pwIpavOCasio7fSnRtVnS1dkp0SasGZI7elVAdcuOR7Iqk59KkuyqtWXhlFxMhOrSNEqsqlOUTZn/SpYmQNXTz4PiHQ0mVotmte1slWFUf3XXh0aWKnVlYNzRdhh4wqSwRK2sPxDTsoYG1DJ1u2W9FtmxyibT83S6lDpG2HEOBtP0V5TUvd0YWYTq1hqqZZF4tJdHh1G2nTOtrSmPV0oFdba1Wp0t659reP6KVvf8hYjte0s8QbXUN2StLila+6JegW3ijJ3n85dEnHhyKus4TS02b3uQb772+36jKzDhS54JSje8ZJXY44l2HG9GV++thewlj2vddPbx7+aqVr+/KqUe9eb3vmONXfpAm51+atf7KIXR+DyVqwC3OAFq3PCNVrXsQyMXwVT+JD5dVq4oqXhDf+ww4CyMDUxLLRlpRbFJoZqiTV1Lxlbq8UffrF6XZziUfItxDbmMI5nGON+CYxOPJbwjYOsEwJH5mJiKvKPh6zkSCbZZk4+EsMiK2DwMtlNmXTRyLSs4ymHpMteDjOHBCfmKpNZnkBu1N2CSDKvjrnNrK2z1uLsxKnRmc12hvFdRUw17qytvFu+rpkVyDYz2o2oh3Zuoh0YOGlO2tF4/vO3Lo2l4anJeU2LNC5BLek9FpjTVhO1K1E96jh25YzcfXRxVb1qLoqTiwr+aZushQprAtm61xsUW641GexZ+7rYQwX2rlc77NEYu9kx3NuyqZpsADq72ndR7rTjGu0MWrvbHw1utsG6bW57u9y7ydy4+Zhucpu72rgN91PXjcp2+/qg8DaqvOdNb1e/Lt9o9Le+981q2N4bqwWXnMA1CD2Ae5HhXEr4+06raUyzVMprhvjlvOfwth4czBivtMY7nlaRS/Xji/4syfWa8pKbfGfnXLlgYc7yll/55ROneE1l7nGaY46CG1/iz8PHcx8bVueTNbqDh85ivyJ9s01PutIjXMOg2/XpO476fadudXdSnZBY3yoSu671m8fr693lONlx7tO0l93swkX+u5/VrlC208vtsW341mmb9yrZvYFc3btHAc/3vsc9gWKX4eH3TPhf/zHxJ3T8FRffNWELPpuQL6nktXPKy/u88nHKvJdIyflvet7IoD9iqEvPzNHz8/QMOanqi8l6Obs+8HRX++yBCXquxx6mvYdU351+e5znPs1KN2jx45d8hGM8ost/5+9n1e6VPj/kwz+gs/Ed/Zxuf1i01nb3Nx/+DOsP0uPX9fUNn3H9Vn977R8Tn038/oWfv+5ZnvL8sZ9+vLfyz/kv3v9dStbJ3T8EIMHtH/oNEgEWCwK2mQE2i1kt4GLV3yQ9oAQ+FgVCkgVeoPJloLQ1IAde4AaSzgj+hqD7eeBSoaAJch8I4p8KrqD4taCSlSAMAuAL1hEN1uABFp4O1osM9qCd5SC4/SAQkpkQghQRFuEM3uDfJaES4tgRYpsTPqH8MeEaRSEVQpsV/tsWZiH/8eATYqEX4loXfuGbjSEaAkwZWpEYpuGprSHcnaEbjmEbkuEUzmGswSHQ6SEeFt0dLlgd9uFb/SH78aEgqh8hDpghHiIGJiKXLSIjdqAjIhokRiL0TaL5YaIlOl8lMp0mbmJCBSKyfSIoHh0pKpYoluJ/deLjsaIq7qAc4mEqvmLbnSL42SItmtMsfpor5iIJ9mLn4aIvWh4wkp4wDuMz7WJTHSMyyl7+MSIiGDbjEjIjTSmjNELgM9pcNF7ji1mjoVEjN8bgNgKhN4ajoI1jD5ajOZ5LNjYiOq7jI4LjyMkjPH7gO9agOtYjquQjXrWjPvZZLM4hP/4jjdGjyhkkQV6hP0riPSakuC0kyiGkQ5qhxaniQE5kolykd0kkRu4hRyIfRHakfX2kKTakSM6jSZqgRp7k54Wk9aUkS8YcSfrTSsZkf7nkCc6kTQYjTHJgTe4kiPWkCOIkUG7LT46kUBYl7+mkPR2lUiqJU+ZLVD6lyhAl/TElVV5iUi7gVGYl1G0lAXalV+4cWMqdWI6lipyluaglWjJfWeKeVbYlO2LlUr6lXKb+WlzaIF3epb3tpS7mJV+umF/anl0GJuUNpnwBpmEWZGFiGlsuJu0FGi0+JmS2HhgNI2VWJiM1pv8ppmZmpGf2W2h+pp9k5rSYJmkqHmcG4WimZksiJjHCpmu+mmwmY2vO5k3W5urdJm4+GW9GDmr2JqH95hCupnBWnW46Y3IeZz8u5y4FJ3N6nXEaIXFGJ/VUpxROp3V+EHQSS3duJ6Np5zSKJ3hCY0C64XeWJ1CRJxRip3rO3HmmYXq+J0NVZCnOJ31SmmS+In7mp2r0p/S5p38an3OmXoEO6LgAKKsoKIIqmn2CIoM2aKexZze6noVeKIbWnC9mKId2qIcmSzP+fqiIjiiJKlUuliiKpqiKgtaJrqiLviiM0k2LxiiN1qiNtgwy3qiO7iiPhg1m9iiQBimNhqiQFqmRliiRHqmSLumFJimTPimU2p2TRimVVmnLTamVZqmWmhuWbqmXfmm95SiYjimZ8tuGlimapmnEiamatqmbgtyZvqmczinPdCmd3imejg6b5imf9ql/7amfBqqgJtePDqqhHirYxSmiLiqjtpOiNiqkRqrfzaikVqqlPugmXqqmbmrlFCqnfiqoPpunhiqplirqTaappqqqStGjrqqrrqqdvqqsgmqszqqtXmqt3qquQmqu7qqvHmqv/qqw+mmwDqux3mmxHqtUsrppsi6rs5Zpsz6rtHpptE6rtVZptV6rtjJptm6rtxZpt36ruPJouI6rudZouZ6rurpouq6ruyIpoL6rvO5ou86rvWJovd6rvu5evO6rv6JougQEADs="""

if __name__ == '__main__':
    import ctypes
    myappid = "marcellomassaro.pysumchecker"
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
    app = QtGui.QApplication(sys.argv)
    win = MainWindow()
    win.show()
    sys.exit(app.exec_())