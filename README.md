# README #

Python script to check and calculate cheksums (MD5, SHA1, SHA2).

### What do I need? ###
Two things:
* Python 3+
* PyQt4

### How does it work? ###

* __On Linux__: fire up a console window (yup, just from the console, for now) and simply type `python3 pysumchecker.py`
* __On Windows__: rename the extension from `.py` to `.pyw` to load it automagically without needing a prompt window. If this does not work, you have to link the `.pyw` file extension with the `pythonw` program inside your python distribution.

### Credits ###
It's too damn simple to be written by more than one person. So just me. :) And obviously all the guys that make Python possible.

The icon that appears in the repository is public domain, available [here](https://thenounproject.com/search/?q=checksum&i=21048).

### License ###
I'm releasing this tool under the [GNU GPL v3](http://www.gnu.org/licenses/gpl-3.0.txt).